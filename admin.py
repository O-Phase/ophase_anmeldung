from django.contrib import admin

from .models import *

class ErstiEmail(admin.TabularInline):
    model = EMail
    extra = 0

class ErstiKommentar(admin.StackedInline):
    model = Kommentar
    extra = 0

class ErstiAdmin(admin.ModelAdmin):
    inlines = [ErstiKommentar, ErstiEmail]
    list_filter = ('veranstaltung','studienfach')
    search_fields = ('vorname', 'nachname')

admin.site.register(Veranstaltung)
admin.site.register(Studienfach)
admin.site.register(Ersti, ErstiAdmin)
