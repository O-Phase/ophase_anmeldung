from django import forms
from django.db import transaction

from .models import EMail, Ersti, Kommentar, Studienfach, Veranstaltung

class AnmeldeForm(forms.Form):
    anrede = forms.CharField(max_length=3,
        widget=forms.Select(choices=Ersti.ANREDE))
    vorname = forms.CharField(max_length=50)
    nachname = forms.CharField(max_length=50)
    email = forms.EmailField()
    studienfach = forms.ModelChoiceField(queryset=Studienfach.objects.all())
    infoverteiler = forms.BooleanField(required=False)
    newsverteiler = forms.BooleanField(required=False)
    kommentar = forms.CharField(widget=forms.Textarea, required=False)

    def clean(self):
        cleaned_data = super(AnmeldeForm, self).clean()
        mail = cleaned_data.get('email')

        if EMail.objects.filter(email=mail).count() > 0:
            self.add_error('email', "E-Mailadresse bereits in Verwendung. "
                "Überprüfe dein Postfach ob du dich schonmal angemeldet hast.")

    @transaction.atomic
    def save(self):
        ersti = Ersti(veranstaltung = Veranstaltung.get_current(),
            anrede = self.cleaned_data['anrede'],
            vorname = self.cleaned_data['vorname'],
            nachname = self.cleaned_data['nachname'],
            studienfach = self.cleaned_data['studienfach'],
            infoverteiler = self.cleaned_data.get('infoverteiler', False),
            newsverteiler = self.cleaned_data.get('newsverteiler', False),
        )
        ersti.save()
        email = EMail(email = self.cleaned_data['email'], ersti = ersti)
        email.save()
        if 'kommentar' in self.cleaned_data and self.cleaned_data['kommentar']:
            kommentar = Kommentar(text = self.cleaned_data['kommentar'],
                ersti = ersti)
            kommentar.save()

        return email

class ErstiForm(forms.ModelForm):
    class Meta:
        model = Ersti
        fields = ['anrede', 'vorname', 'nachname', 'studienfach',
            'infoverteiler', 'newsverteiler']

class KommentarForm(forms.Form):
    text = forms.CharField(widget=forms.Textarea)
    text.label = 'Neuer Kommentar'
