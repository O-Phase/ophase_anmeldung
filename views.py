from django.core.urlresolvers import reverse
from django.http import Http404
from django.contrib.messages.views import SuccessMessageMixin
from django.views.generic import View, DetailView, FormView, UpdateView, TemplateView
from django.views.generic.detail import SingleObjectMixin
from django.shortcuts import render

from .forms import AnmeldeForm, ErstiForm, KommentarForm
from .models import EMail, Ersti
from .mail import sendRegistrationMail

class AnmeldeView(SuccessMessageMixin, FormView):
    template_name = 'ophase_anmeldung/anmeldung.html'
    form_class = AnmeldeForm
    success_message = """Hallo %(vorname)s %(nachname)s, vielen Dank für deine Anmeldung.<br /><br />
Um die Anmeldung abzuschließen musst du noch deine E-Mailadresse bestätigen. Wir haben dir dafür eine E-Mail zugeschickt, klicke darin auf den entsprechenden Link.  (Sollte in den nächsten 12 Stunden keine E-Mail ankommen, prüfe bitte deinen Spam-Ordner und den freien Speicher)<br /><br />
Solltest du Fragen haben schreibe uns eine E-Mail an ophase@fs.lmu.de"""

    def get(self, request, *args, **kwargs):
        form = self.form_class(initial=self.initial)
        return render(request, self.template_name, {'form': form})

    def get_success_url(self):
        return reverse('ophase_anmeldung:anmeldung')

    def form_valid(self, form):
        email = form.save()
        sendRegistrationMail(email)
        return super(AnmeldeView, self).form_valid(form)

class AddedView(TemplateView):
    template_name = 'ophase_anmeldung/added.html'

class ErstiDisplay(DetailView):
    model = Ersti
    template_name = 'ophase_anmeldung/ersti.html'

    def get_object(self):
        try:
            email = EMail.from_link(self.args[0])
        except (EMail.DoesNotExist, ValueError):
            raise Http404("Link not valid")

        if not email.verifiziert:
            # TODO gegebenenfalls alte Mails entfernen
            email.verifiziert = True
            email.save()

        return email.ersti

    # add the KommentarForm to the context as a field to add a new comment
    def get_context_data(self, **kwargs):
        context = super(ErstiDisplay, self).get_context_data(**kwargs)
        context['form'] = KommentarForm()
        context['uid'] = self.args[0]
        return context

class ErstiKommentar(SingleObjectMixin, SuccessMessageMixin, FormView):
    model = Ersti
    template_name = 'ophase_anmeldung/ersti.html'
    form_class = KommentarForm
    success_message = 'Kommentar hinzugefügt'
    success_url = '.'

    def get_object(self):
        try:
            email = EMail.from_link(self.args[0])
        except (EMail.DoesNotExist, ValueError):
            raise Http404("Link not valid")

        return email.ersti

    def get_context_data(self, **kwargs):
        context = super(ErstiKommentar, self).get_context_data(**kwargs)
        context['uid'] = self.args[0]
        return context

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(ErstiKommentar, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        self.object.kommentar_set.create(text=form.cleaned_data['text'])
        return super(ErstiKommentar, self).form_valid(form)

class ErstiView(View):
    def get(self, request, *args, **kwargs):
        return ErstiDisplay.as_view()(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return ErstiKommentar.as_view()(request, *args, **kwargs)

class ErstiEditView(SuccessMessageMixin, UpdateView):
    template_name = 'ophase_anmeldung/ersti_edit.html'
    model = Ersti
    form_class = ErstiForm
    success_message = "Änderungen wurden gespeichert"

    def get_success_url(self):
        return reverse('ophase_anmeldung:ersti', args=(self.args[0],))

    def get_object(self):
        try:
            email = EMail.from_link(self.args[0])
        except (EMail.DoesNotExist, ValueError):
            raise Http404("Link not valid")

        return email.ersti

    def get_context_data(self, **kwargs):
        context = super(ErstiEditView, self).get_context_data(**kwargs)
        context['uid'] = self.args[0]
        return context
